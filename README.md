
# Jason's Personal Website

## Introduction
Welcome to the repository for my personal website! This site showcases my projects, skills, and experiences as an Electrical and Computer Engineering (ECE) student with a passion for cloud computing and AWS. The website is designed to be minimalistic, user-friendly, and responsive, ensuring accessibility on various devices and screen sizes.

## Features
- **Professional Profile**: Includes a professional headshot and a brief introduction about my background and interests.
- **Project Showcase**: Detailed sections for each of my projects with summaries, technologies used, and links to live demos where applicable.
- **Responsive Design**: Crafted to provide an optimal viewing experience—easy reading and navigation with a minimum of resizing, panning, and scrolling—across a wide range of devices.

## Technologies Used
- **HTML5**: For structuring content on the website.
- **CSS3**: For styling, layout, and responsive design.
- **Jinja2 Templating**: For dynamically inserting content into the HTML templates.
- **Static Site Generation**: Utilizing a static site generator for pre-building pages for faster load times.

## Setup and Installation
1. **Clone the Repository**
   ```sh
   git clone https://....
   ```
2. **Navigate to the Project Directory**
   ```sh
   cd jason-personal-website
   ```
3. **Install Dependencies** (If applicable)
   ```sh
   npm install
   ```
4. **Run the Development Server** (If applicable)
   ```sh
   npm start
   ```
   Your site should now be running at `http://localhost:3000`.

## Structure
- `index.html`: The main entry point of the website.
- `static/`: Contains all the static assets like CSS, images, and JavaScript files.
- `templates/`: Jinja2 templates for rendering dynamic content.
- `projects/`: Markdown files representing individual project details.

## Screen shots:
- Home Page
    - ![Alt text](./images/readme0.jpeg "Optional title0")

- Project Page
    - ![Alt text](./images/readme1.jpeg "Optional title0")

- About Page
    - ![Alt text](./images/readme2.jpeg "Optional title0")

   
Thank you for visiting my personal website project repository!
