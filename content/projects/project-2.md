+++
title = "Continuous Delivery of Personal Website"
date = 2023-02-18
template = "project.html"

[extra]
project_url = "https://gitlab.com/zd592/ids721-project4"
+++

This is my second project. Check it out!

<!-- more -->

## Prerequisites

- Rust programming language and Cargo package manager installed.
- AWS CLI configured with appropriate AWS account credentials.
- [Optional] Docker for testing Lambda functions locally.

## Setup and Installation

### 2. Install Dependencies

Ensure your `Cargo.toml` has the necessary dependencies for AWS SDK for Rust (or Rusoto if you're using it), Lambda runtime, and any other libraries you're using.

```toml
[dependencies]
# Example dependencies
aws-sdk-dynamodb = "0.13.0"
lambda_runtime = "0.4.1"
tokio = { version = "1", features = ["full"] }
```

Run `cargo build` to download and compile dependencies.

### 3. Environment Configuration

Set up environment variables for local development, if necessary. For example, to specify the AWS region and DynamoDB table name:

```bash
export AWS_REGION='us-east-1'
export DYNAMODB_TABLE='yourTableName'
```

### 4. Write Your Lambda Function

Implement your Lambda function logic in Rust. Use the AWS SDK for Rust to interact with DynamoDB within your function.

### 5. Build Your Lambda Function

Compile your Lambda function targeting the AWS Lambda Linux runtime.

```bash
cargo lambda build --release --target x86_64-unknown-linux-musl
```

### 6. Deploy Your Lambda Function

Deploy your function to AWS Lambda using the AWS CLI or the `cargo lambda` tool.

```bash
cargo lambda deploy --iam-role arn:aws:iam::ID:role/YourLambdaExecutionRole
```

### 7. Test Your Lambda Function

Invoke your Lambda function using the AWS CLI or through the AWS Management Console to ensure it behaves as expected.

## Testing

Describe how to run automated tests for this system. Mention any frameworks used.

```bash
cargo test
```

## Deployment

Additional notes about how to deploy this on a live system.
